import React, {useRef} from 'react';

import { Editor } from '@tinymce/tinymce-react';

import '@wiris/mathtype-tinymce5';

import './App.css';



function App() {
  const editorRef = useRef();
  const handleEditorChange = (e) => {
    console.log(
      'Content was updated:', e
    );
    console.log(e.target.getContent())
  }
  
  function handlerFunction(a,b,c){
    console.log(a)
  }

  return (
    <div className="App" onClick={(a)=>{console.log(a)}}>
      <Editor
        initialValue="<p>Initial content</p>"
        ref={editorRef}
        // apiKey="z5isf3wmyuf83lqgu4xc8bag5fwavxg29xwgrbup01ea7yqa"
        init={{

          //setup fixes editor focus on click of toolbar buttons
          
          setup: function (editor) {editor.on('ExecCommand', function (e) {editor.focus(); });},
          height: 500,
          menubar: false,
          selector: 'textarea',
          external_plugins: { tiny_mce_wiris: `${window.location.href}/node_modules/@wiris/mathtype-tinymce5/plugin.min.js` },
          plugins: [
              'advlist autolink lists link image ', 
              'charmap print preview anchor help',
              'searchreplace visualblocks code',
              'insertdatetime media table paste wordcount imagetools'
            ],

          //Tinymce wiris formulae editor added 
         toolbar:
            'undo redo | formatselect | bold italic image| \
            alignleft aligncenter alignright | \
            bullist numlist outdent indent | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry | help',
          // images_upload_url: 'https://preprodms.embibe.com/ask/v1/upload?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTIzMTgyNTE1MiwiZW1haWwiOiJkc2xfdGVzdGluZzhkZW1vQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOjUxMywiaXNfZ3Vlc3QiOmZhbHNlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTAyLTI4VDEyOjQ3OjQ3LjgwNloifQ.GLvVTVIkB29REHx0GomnxNJQKZmGlmJOjqgFkY5cxyepOFk-IdHT2CU9xhNfYf_l4uKNL9XydlpBjzsShJB1iA',
          imagetools_cors_hosts: ['development.embibe.com','moxiecode.cachefly.net', 'otherdomain.com'],
          // image upload api cal has been handled
          images_upload_handler:  (blobInfo, success, failure) => {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', 'https://preprodms.embibe.com/ask/v1/upload?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTIzMTgyNTE1MiwiZW1haWwiOiJkc2xfdGVzdGluZzhkZW1vQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOjUxMywiaXNfZ3Vlc3QiOmZhbHNlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTAyLTI4VDEyOjQ3OjQ3LjgwNloifQ.GLvVTVIkB29REHx0GomnxNJQKZmGlmJOjqgFkY5cxyepOFk-IdHT2CU9xhNfYf_l4uKNL9XydlpBjzsShJB1iA')
            xhr.onload = function() {
              var json;
              if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
              }
              json = JSON.parse(xhr.responseText);
              json.location=json.url; 
              if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
              }

              success(json.location);
            };
            formData = new FormData();
            formData.append('upload', blobInfo.blob(), blobInfo.filename());
        
            xhr.send(formData);
          },
          imagetools_proxy:'',
          imagetools_cors_hosts:['askembibe.blob.core.windows.net', 'development.embibe.com', 'embibe.com', 'preprod.embibe.com'],
        }}
        
        onSelectionChange={handlerFunction}
        onClick={(e)=>{console.log(e)}}
        onChange={handleEditorChange}
        onEditorChange={(e)=>{console.log(e)}}
      />
    </div>
  );
}

export default App;
